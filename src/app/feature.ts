export interface Feature {
    name: string;
    subinformation: Array<string>;
}

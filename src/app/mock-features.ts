import { Feature } from './feature';
export const FEATURES: Feature[] = [
    { name: "Register for an Account", subinformation: ["Allows the user to create an account", "User will provide name, bio, ZIP code, whether or not they have pets, desired living size"] },
    { name: "log in", subinformation: ["Allows the user to log in to the site", "User will provide username and password"] },
    { name: "Search for Matches", subinformation: ["User will be able to filter roommates based on the criteria they set", "Examples of such criteria include pets, distance, etc."] }
];

import { Component, OnInit } from '@angular/core';
import { Feature } from '../feature';
import { FEATURES } from '../mock-features';
@Component({
  selector: 'app-feature',
  templateUrl: './feature.component.html',
  styleUrls: ['./feature.component.css']
})
export class FeatureComponent implements OnInit {
 features = FEATURES;
 selectedFeature: Feature;

  constructor() { }

  ngOnInit(): void {
  }
  onSelect(feature: Feature): void {
    this.selectedFeature = feature;
  }

}
